/**
 * CarMaintenanceController
 *
 * @description :: Server-side logic for managing Carmaintenances
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	add_maint: function(req, res){
		var new_maint = {};
		new_maint.mileage = req.body.mileage;
		new_maint.car = req.body.car;
		new_maint.date = req.body.date;
		new_maint.type = req.body.type.id;
		CarMaintenance.create(new_maint).exec(function(err, result){
			res.json(result);
		});
	}
};

