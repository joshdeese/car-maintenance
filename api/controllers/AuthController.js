/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var passport = require('passport');

module.exports = {
	signin: function(req, res) {
		req.session.redirect = req.header('Referer') || '/account';
		res.view();
	},
	signout: function (req, res) {
		req.logout();
		return res.redirect('/');
	},
	signup: function(req, res){
		res.view();
	},
	add_user: function(req, res){
		delete req.body.conf_password;
		User.create(req.body).exec(function(err, result){
			res.json({text: 'done'});
		})
	},
	process: function(req, res) {
		passport.authenticate('local', function(err, user, info) {
			if (err)   {
				console.log('Auth Error');
				return res.redirect('/signin');
			}
			if ( ! user) {
				console.log('User Error');
				req.flash('danger', 'Email or Password incorrect, please try again.');
				return res.redirect('/signin');
			}

			req.logIn(user, function(err) {
				if (err) {
					return res.redirect('/signin');
				}

				User.findOne(req.session.passport.user).populate('roles').exec(function(err, result){
					var redirect;
					var myRoles = result.roles;
					var isAdmin = false;
					
					for(var i=0; i<myRoles.length; i++){
						if(myRoles[i].type == 'admin'){
							isAdmin = true;
						}
					}
					
					redirect = req.session.redirect;
					
					delete req.session.redirect;
	
					res.redirect(redirect);
				});
			});
		})(req, res);
	},
};