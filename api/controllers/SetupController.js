/**
 * SetupController
 *
 * @description :: Server-side logic for managing setups
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	carMakeDB: function(){
		var carMake = [{
			name: 'Nissan'
		}, {
			name: 'Kia'
		}, {
			name: 'Lexis'
		}, {
			name: 'Audi'
		}, {
			name: 'Millenium'
		}, {
			name: 'DeLorean'
		}, {
			name: 'Ferrari'
		}, {
			name: 'Aston Martin'
		}, {
			name: 'BMW'
		}, {
			name: 'Jaguar'
		}, {
			name: 'Jeep'
		}, {
			name: 'Lamborghini'
		}, {
			name: 'Chevrolet'
		}, {
			name: 'Ford'
		}, {
			name: 'Honda'
		}];
		
		CarMake.create(carMake).exec(console.log);
	},
	
	primeDB: function(res, req){
		var storeUsers = [];
		
		var roles = [{
			type: 'user'
		}, {
			type: 'admin'
		}];
		
		var users = [{
			nameFirst: 'Andy',
			nameLast: 'Dwyer',
			email: 'adwyer@pawnee.gov',
			password: 'password'
		}, {
			nameFirst: 'Tom',
			nameLast: 'Haverford',
			email: 'thaverford@pawnee.gov',
			password: 'password'
		}, {
			nameFirst: 'Sheldon',
			nameLast: 'Cooper',
			email: 'scooper@caltech.edu',
			password: 'password'
		}, {
			nameFirst: 'Leonard',
			nameLast: 'Hofstadter',
			email: 'lhofstadter@caltech.edu',
			password: 'password'
		}, {
			nameFirst: 'Princess',
			nameLast: 'Cinderella',
			email: 'cinder@enchantedforest.com',
			password: 'password'
		}, {
			nameFirst: 'Luke',
			nameLast: 'Skywalker',
			email: 'lskywalker@therebellion.net',
			password: 'password'
		}, {
			nameFirst: 'Han',
			nameLast: 'Solo',
			email: 'hsolo@falconmovers.com',
			password: 'password'
		}, {
			nameFirst: 'Aragorn II',
			nameLast: 'Son of Arathorn',
			email: 'aragorn@gondor.gov',
			password: 'password'
		}, {
			nameFirst: 'Kvothe',
			nameLast: 'The Bloodless',
			email: 'kote@waystoneinn.com',
			password: 'password'
		}, {
			nameFirst: 'Ferris',
			nameLast: 'Beuller',
			email: 'ferris@beuller.net',
			password: 'password'
		}];
		
		var cars = [{
			// Nissan
			make: '543bc3ddad7ae86a1a6f090c',
			model: 'Altima',
			model_year: '2013',
			bought_date: '9/12/12'
		}, {
			// Kia
			make: '543bc3ddad7ae86a1a6f090d',
			model: 'Sorento',
			model_year: '2006',
			bought_date: '7/22/2006'
		}, {
			// Lexis
			make: '543bc3ddad7ae86a1a6f090e',
			model: 'LFA',
			model_year: '2012',
			bought_date: '5/11/2012'
		}, {
			// Audi
			make: '543bc3ddad7ae86a1a6f090f',
			model: 'R8',
			model_year: '2015',
			bought_date: '10/7/14'
		}, {
			// Millenium
			make: '543bc3ddad7ae86a1a6f0910',
			model: 'Falcon',
			model_year: '1977',
			bought_date: '5/25/1977'
		}, {
			// DeLorean
			make: '543bc3ddad7ae86a1a6f0911',
			model: 'DMC 12',
			model_year: '1981',
			bought_date: '7/3/1985'
		}, {
			// Ferrari
			make: '543bc3ddad7ae86a1a6f0912',
			model: '250 GT Spyder',
			model_year: '1961',
			bought_date: '6/11/1986'
		}, {
			// Aston Martin
			make: '543bc3ddad7ae86a1a6f0913',
			model: 'DB5',
			model_year: '1964',
			bought_date: ''
		}, {
			// BMW
			make: '543bc3ddad7ae86a1a6f0914',
			model: '325i',
			model_year: '2003',
			bought_date: ''
		}, {
			// Jaguar
			make: '543bc3ddad7ae86a1a6f0915',
			model: 'XJ L',
			model_year: '2011',
			bought_date: ''
		}, {
			// Jeep
			make: '543bc3ddad7ae86a1a6f0916',
			model: 'Grand Cherokee',
			model_year: '1999',
			bought_date: ''
		}, {
			// Lamborghini
			make: '543bc3ddad7ae86a1a6f0917',
			model: 'Murcielago LP640',
			model_year: '2008',
			bought_date: ''
		}, {
			// Chevrolet
			make: '543bc3ddad7ae86a1a6f0918',
			model: 'Camaro',
			model_year: '2012',
			bought_date: ''
		}, {
			// Ford
			make: '543bc3ddad7ae86a1a6f0919',
			model: 'Mustang',
			model_year: '1966',
			bought_date: ''
		}, {
			// Honda
			make: '543bc3ddad7ae86a1a6f091a',
			model: 'Accord',
			model_year: '1995',
			bought_date: ''
		}];
		
		var associate = function(oneCar, cb){
			var thisCar = oneCar;
			var callback = cb;
			var rnd = Math.floor(Math.random()*storeUsers.length);
			var thisUser = storeUsers[rnd];
			thisUser.cars.add(thisCar.id);
			thisUser.save(console.log);
		}
		
		var afterCar = function(err, newCars){
			while(newCars.length){
				var thisCar = newCars.pop();
				var callback = function(carID){
					console.log('Done with car ', carID);
				}
				associate(thisCar, callback);
			}
			console.log('Done w/ associations');
		};
		
		var afterUser = function(err, newUsers){
			while(newUsers.length)
				storeUsers.push(newUsers.pop());
			
			Car.create(cars).exec(afterCar);
		};
		
		Role.create(roles).exec(console.log);
		User.create(users).exec(afterUser);
	},
	
	resetDB: function(res, req){
		User.destroy().exec(console.log);
		Car.destroy().exec(console.log);
	},
	
	user_cars: function(res, req){
		User.find().populate('cars').exec(function(err, users){
			for(var i=0; i<users.length; i++){
				user = users[i];
				for(var j=0; j<user.cars.length; j++){
					Car.findOne(user.cars[j].id).populate('make').exec(function(err, car){
						console.log('Name: ' + user.nameFirst + ' ' + user.nameLast);
						console.log('Car '+j);
						console.log('Make: ' + car.make.name);
						console.log('Model: ' + car.model);
					});
				}
			}
		});
	},
};