/**
 * AccountController
 *
 * @description :: Server-side logic for managing accounts
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var moment = require('moment');

module.exports = {
	index: function(req, res){
		res.view();
	},
	
	cars: function(req, res){
		Car.find({owner: req.user[0].id}).populate('make').exec(function(err, cars){
			res.view({result: JSON.stringify(cars)});
		});
	},
	
	car_maint: function(req, res){
		CarMaintenance.find({where: {car: req.query.id}, sort: 'date'}).populate('type').exec(function(err, maintenance){
			console.log(maintenance);
			Car.findOne(req.query.id).populate('make').exec(function(err, car){
				console.log(car);
				res.view({maintenance:JSON.stringify(maintenance), carID:req.query.id, car: car, moment:moment});
			});
		});
	},
};

