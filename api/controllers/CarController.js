/**
 * CarController
 *
 * @description :: Server-side logic for managing cars
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	add_car: function(req, res){
		var new_car = req.body.car;
		new_car.owner=req.body.user;
		
		console.log(new_car);
		
		Car.create(new_car).exec(function(err, car){
			Car.findOne(car.id).populate('make').exec(function(err, myCar){
				res.json(myCar);
			})
		});	
	},
};

