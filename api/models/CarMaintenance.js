/**
* CarMaintenance.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
		date: {
			type: 'date'
		},
		type: {
			model: 'maintenance'
		},
		car: {
			model: 'car'
		},
		mileage: {
			type: 'integer'
		},
		description: {
			type: 'string'
		}
  }
};

