/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var bcrypt = require('bcrypt');

module.exports = {
  attributes: {
		email: {
			type: 'string',
			required: true,
			unique: true
		},
		password: {
			type: 'string',
			required: true,
			minLength: 6
		},
		nameFirst: {
			type: 'string',
			required: true
		},
		nameLast: {
			type: 'string',
			required: true
		},
		cars: {
			collection: 'car',
			via: 'owner'
		},
		roles: {
			collection: 'role',
			via: 'user'
		},
		toJSON: function() {
			var obj = this.toObject();
			delete obj.password;
			return obj;
		},
  },
  
  beforeCreate: function(user, cb) {
		bcrypt.genSalt(10, function(err, salt) {
			bcrypt.hash(user.password, salt, function(err, hash) {
				if (err) { return cb(err); }

				user.password = hash;
				cb();
			});
		});
	},
};

