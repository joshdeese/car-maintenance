/**
* Car.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
		make: {
			model: 'carmake'
		},
		model: {
			type: 'string'
		},
		model_year: {
			type: 'integer'
		},
		bought_date: {
			type: 'date'
		},
		vin: {
			type: 'string'
		},
		licensePlate: {
			type: 'string'
		},
		owner: {
			model: 'user'
		},
		maintenance: {
			collection: 'carmaintenance',
			via: 'car'
		}
  }
};

