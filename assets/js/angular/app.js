'use strict';

var app = angular.module('car_stats', []);

app.controller('cars', function($scope, $http, $window){
	$scope.myCars = $window.cars;
	$http.get('/carmake').success(function(data, status, headers, config){
		$scope.make_list = data;
	}).error(function(data, status, headers, config){
		console.log(data);
	});
	
	$scope.add_car = function(){
		console.log($window.user);
		$http.post('/car/add_car', {car: $scope.new_car, user: $window.user}).success(function(data, status, headers, config){
			console.log(data);
			$scope.myCars.push(data);
		}).error(function(data, status, headers, config){
			console.log(data);
		});
		$scope.new_car = {};
	}
});

app.controller('car_maint', function($scope, $http, $window){
	$scope.maintenance = $window.maintenance;
	for(var i=0; i<$scope.maintenance.length; i++){
		$scope.maintenance[i].pretty_date = moment($scope.maintenance[i].date).format('MM/DD/YYYY');
	}
	
	$scope.new_maint = {};
	
	$http.get('/maintenance').success(function(data, status, headers, config){
		$scope.maint_types = data;
	}).error(function(data, status, headers, config){
		console.log(data);
	});
	
	$scope.save_maint = function(){
		$scope.new_maint.car = $window.carID;
		$http.post('/carmaintenance/add_maint', $scope.new_maint).success(function(data, status, headers, config){
			$scope.new_maint.pretty_date = moment($scope.new_maint.date).format('MM/DD/YYYY');
			$scope.maintenance.push($scope.new_maint);
			$scope.new_maint = {};
		}).error(function(data, status, headers, config){
			console.log(data);
		});
	}
});

app.controller('signin', function($scope, $http){
	
});

app.controller('signup', function($scope, $http, $window){
	$scope.user = {};
	
	$scope.signup = function(){
		if($scope.user.password == $scope.user.conf_password){
			$http.post('/auth/add_user', $scope.user).success(function(data, status, headers, config){
				if(data){
					$http.post('/signin', {email: $scope.user.email, password: $scope.user.password}).success(function(data, status, headers, config){
						// go to account page
						$window.location.href = "/account";
					}).error(function(data, status, headers, config){
						console.log(data);
					});
				} else {
					alert('some error has occured');
				}
			}).error(function(data, status, headers, config){
				console.log(data);
			});
		} else {
			alert("Please make sure that both password fields match");
		}
	};
});