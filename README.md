# Car Maintenance

a [Sails](http://sailsjs.org) application

This application will be used to track maintenance and mileage on your cars!

v1.0 planned feature set
<ul>
<li>Log in with passport (use auth from facebook, google, etc also)</li>
<li>Add your cars</li>
<li>Manually enter maintenance that is preformed on your cars</li>
<li>Track mileage and gas consumption each time you pump gas</li>
<li>View charts</li>
  <ul>
  <li>Gas mileage over time</li>
  <li>Miles driven over time</li>
  </ul>
</ul>

Feature Ideas
- Remind you when maintenance needs to be preformed
- Use ODB2 Scanner to automatically export mileage data to the website